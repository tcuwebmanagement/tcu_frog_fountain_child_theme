<?php
/**
 * Tabs page template
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

add_action( 'acf/init', 'tcu_academic_tabs_fields' );

/**
 * ACF callback function
 */
function tcu_academic_tabs_fields() {
	acf_add_local_field_group(
		array(
			'key'                   => 'group_592da2dd6976c',
			'title'                 => 'Tabs page template',
			'fields'                => array(
				array(
					'key'               => 'field_592da2eb77049',
					'label'             => 'Tabs',
					'name'              => 'tabs_repeater',
					'type'              => 'repeater',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'collapsed'         => 'field_592da39a7704a',
					'min'               => 0,
					'max'               => 0,
					'layout'            => 'block',
					'button_label'      => '+ Add Tab',
					'sub_fields'        => array(
						array(
							'key'               => 'field_592da39a7704a',
							'label'             => 'Tab Title',
							'name'              => 'tab_title',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
						array(
							'key'               => 'field_592da3d37704b',
							'label'             => 'Tab Content',
							'name'              => 'tab_content',
							'type'              => 'wysiwyg',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'tabs'              => 'all',
							'toolbar'           => 'full',
							'media_upload'      => 1,
							'delay'             => 0,
						),
					),
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'page_template',
						'operator' => '==',
						'value'    => 'page-tabs.php',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => array(
				0 => 'the_content',
			),
			'active'                => 1,
			'description'           => '',
		)
	);
}
