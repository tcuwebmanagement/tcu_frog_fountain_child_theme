<?php
/**
 * TCU Alert Options Page
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

add_action( 'acf/init', 'tcu_alert_init' );

/**
 * ACF callback function
 */
function tcu_alert_init() {

	if ( function_exists( 'acf_add_options_page' ) ) {

		$option_page = acf_add_options_page(
			array(
				'page_title' => __( 'TCU Alert', 'tcu_frog_fountain_child_theme' ),
				'menu_slug'  => 'theme-general-settings',
				'capability' => 'manage_options',
				'icon_url'   => 'dashicons-warning',
				'position'   => '2.2',
			)
		);

	}
}


