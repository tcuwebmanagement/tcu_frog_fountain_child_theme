<?php
/**
 * Flexible Content ACF fields
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

add_action( 'acf/init', 'tcu_frog_fountain_profiles_template_fields' );

/**
 * ACF callback function
 */
function tcu_frog_fountain_profiles_template_fields() {

	acf_add_local_field_group(
		array(
			'key'                   => 'group_5acb73a47678a',
			'title'                 => 'Leadership Template - Frog Fountain Theme',
			'fields'                => array(
				array(
					'key'               => 'field_5acb77b93e6bf',
					'label'             => 'Profile Section',
					'name'              => 'leadership_template_profile_section',
					'type'              => 'flexible_content',
					'instructions'      => 'Add a profile section to the bottom of the page',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'layouts'           => array(
						'5acb77d88015d' => array(
							'key'        => '5acb77d88015d',
							'name'       => 'leadership_template_profiles',
							'label'      => 'Profiles',
							'display'    => 'block',
							'sub_fields' => array(
								array(
									'key'               => 'field_5acb79253e6c0',
									'label'             => 'Top Content',
									'name'              => '',
									'type'              => 'tab',
									'instructions'      => '',
									'required'          => 0,
									'conditional_logic' => 0,
									'wrapper'           => array(
										'width' => '',
										'class' => '',
										'id'    => '',
									),
									'placement'         => 'top',
									'endpoint'          => 0,
								),
								array(
									'key'               => 'field_5acb794e3e6c1',
									'label'             => 'Title',
									'name'              => 'leadership_template_profiles_title',
									'type'              => 'text',
									'instructions'      => '',
									'required'          => 1,
									'conditional_logic' => 0,
									'wrapper'           => array(
										'width' => '',
										'class' => '',
										'id'    => '',
									),
									'default_value'     => '',
									'placeholder'       => '',
									'prepend'           => '',
									'append'            => '',
									'maxlength'         => '',
								),
								array(
									'key'               => 'field_5acb796f3e6c2',
									'label'             => 'Content',
									'name'              => 'leadership_template_profiles_content',
									'type'              => 'textarea',
									'instructions'      => '',
									'required'          => 0,
									'conditional_logic' => 0,
									'wrapper'           => array(
										'width' => '',
										'class' => '',
										'id'    => '',
									),
									'default_value'     => '',
									'placeholder'       => '',
									'maxlength'         => '',
									'rows'              => '',
									'new_lines'         => 'wpautop',
								),
								array(
									'key'               => 'field_5acb79ad3e6c3',
									'label'             => 'Profiles',
									'name'              => '',
									'type'              => 'tab',
									'instructions'      => '',
									'required'          => 0,
									'conditional_logic' => 0,
									'wrapper'           => array(
										'width' => '',
										'class' => '',
										'id'    => '',
									),
									'placement'         => 'top',
									'endpoint'          => 0,
								),
								array(
									'key'               => 'field_5acb79bd3e6c4',
									'label'             => 'Profiles Repeater',
									'name'              => 'leadership_template_profiles_profiles_repeater',
									'type'              => 'repeater',
									'instructions'      => 'Click on "+ Add Profile" to add a leadership member',
									'required'          => 0,
									'conditional_logic' => 0,
									'wrapper'           => array(
										'width' => '',
										'class' => '',
										'id'    => '',
									),
									'collapsed'         => 'field_5acb7a403e6c6',
									'min'               => 0,
									'max'               => 0,
									'layout'            => 'block',
									'button_label'      => '+ Add Profile',
									'sub_fields'        => array(
										array(
											'key'          => 'field_5acb79fd3e6c5',
											'label'        => 'Image',
											'name'         => 'leadership_template_profiles_image',
											'type'         => 'image',
											'instructions' => 'Add an image size of 200X150 pixels',
											'required'     => 1,
											'conditional_logic' => 0,
											'wrapper'      => array(
												'width' => '',
												'class' => '',
												'id'    => '',
											),
											'return_format' => 'array',
											'preview_size' => 'tcu-200-150',
											'library'      => 'all',
											'min_width'    => '',
											'min_height'   => '',
											'min_size'     => '',
											'max_width'    => '',
											'max_height'   => '',
											'max_size'     => '',
											'mime_types'   => '',
										),
										array(
											'key'          => 'field_5acb7a403e6c6',
											'label'        => 'Full Name',
											'name'         => 'leadership_template_profiles_full_name',
											'type'         => 'text',
											'instructions' => '',
											'required'     => 1,
											'conditional_logic' => 0,
											'wrapper'      => array(
												'width' => '',
												'class' => '',
												'id'    => '',
											),
											'default_value' => '',
											'placeholder'  => '',
											'prepend'      => '',
											'append'       => '',
											'maxlength'    => '',
										),
										array(
											'key'          => 'field_5acb7a603e6c7',
											'label'        => 'Title',
											'name'         => 'leadership_template_profiles_title',
											'type'         => 'text',
											'instructions' => '',
											'required'     => 0,
											'conditional_logic' => 0,
											'wrapper'      => array(
												'width' => '',
												'class' => '',
												'id'    => '',
											),
											'default_value' => '',
											'placeholder'  => '',
											'prepend'      => '',
											'append'       => '',
											'maxlength'    => '',
										),
										array(
											'key'          => 'field_5acb7b719f46b',
											'label'        => 'Email',
											'name'         => 'leadership_template_profiles_email',
											'type'         => 'email',
											'instructions' => '',
											'required'     => 0,
											'conditional_logic' => 0,
											'wrapper'      => array(
												'width' => '',
												'class' => '',
												'id'    => '',
											),
											'default_value' => '',
											'placeholder'  => '',
											'prepend'      => '',
											'append'       => '',
										),
										array(
											'key'          => 'field_5acb7ba29f46c',
											'label'        => 'Phone Number',
											'name'         => 'leadership_template_profiles_phone_number',
											'type'         => 'text',
											'instructions' => '',
											'required'     => 0,
											'conditional_logic' => 0,
											'wrapper'      => array(
												'width' => '',
												'class' => '',
												'id'    => '',
											),
											'default_value' => '',
											'placeholder'  => '',
											'prepend'      => '',
											'append'       => '',
											'maxlength'    => '',
										),
										array(
											'key'          => 'field_5acb7a713e6c8',
											'label'        => 'Link',
											'name'         => 'leadership_template_profiles_link',
											'type'         => 'url',
											'instructions' => '',
											'required'     => 0,
											'conditional_logic' => 0,
											'wrapper'      => array(
												'width' => '',
												'class' => '',
												'id'    => '',
											),
											'default_value' => '',
											'placeholder'  => '',
										),
									),
								),
							),
							'min'        => '',
							'max'        => '',
						),
					),
					'button_label'      => 'Add Profile Section',
					'min'               => '',
					'max'               => '',
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'page_template',
						'operator' => '==',
						'value'    => 'page-leadership.php',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => 1,
			'description'           => '',
		)
	);

}


