<?php
/**
 * TCU Alert - ACF Generated Fields
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

add_action( 'acf/init', 'tcu_frog_fountain_theme_alert_fields' );

/**
 * ACF callback function
 */
function tcu_frog_fountain_theme_alert_fields() {

	acf_add_local_field_group(
		array(
			'key'                   => 'group_58dd2eee5aed0',
			'title'                 => 'TCU Alert - Frog Fountain Theme',
			'fields'                => array(
				array(
					'key'               => 'field_58dd300b7997a',
					'label'             => 'Display Alert',
					'name'              => '',
					'type'              => 'tab',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'placement'         => 'top',
					'endpoint'          => 0,
				),
				array(
					'key'               => 'field_58dd303d7997c',
					'label'             => 'Display Alert Message?',
					'name'              => 'tcu_alert_display_alert_message',
					'type'              => 'true_false',
					'instructions'      => 'WARNING: This message will display below the main navigation. Only use this section in case of emergency.

	If checked, the "Content" tab will appear automatically. Fill out the fields to finish displaying the alert message.',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'message'           => '',
					'default_value'     => 0,
					'ui'                => 0,
					'ui_on_text'        => '',
					'ui_off_text'       => '',
				),
				array(
					'key'               => 'field_58dd30287997b',
					'label'             => 'Content',
					'name'              => '',
					'type'              => 'tab',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => array(
						array(
							array(
								'field'    => 'field_58dd303d7997c',
								'operator' => '==',
								'value'    => '1',
							),
						),
					),
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'placement'         => 'top',
					'endpoint'          => 0,
				),
				array(
					'key'               => 'field_58dd2f09f1ef2',
					'label'             => 'Title',
					'name'              => 'tcu_alert_title',
					'type'              => 'text',
					'instructions'      => '',
					'required'          => 1,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
				),
				array(
					'key'               => 'field_58dd2f30f1ef3',
					'label'             => 'Message',
					'name'              => 'tcu_alert_message',
					'type'              => 'wysiwyg',
					'instructions'      => '',
					'required'          => 1,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '',
					'tabs'              => 'text',
					'toolbar'           => 'basic',
					'media_upload'      => 0,
					'delay'             => 0,
				),
				array(
					'key'               => 'field_58dd2f70f1ef4',
					'label'             => 'Link',
					'name'              => 'tcu_alert_link',
					'type'              => 'url',
					'instructions'      => '',
					'required'          => 1,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '',
					'placeholder'       => '',
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => 'theme-general-settings',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => array(
				0 => 'featured_image',
			),
			'active'                => 1,
			'description'           => '',
		)
	);
}


