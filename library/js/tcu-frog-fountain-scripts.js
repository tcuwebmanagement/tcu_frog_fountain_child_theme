// as the page loads, call these scripts
jQuery( document ).ready( function( $ ) {
    var arrowButton = $( '.tcu-arrow' );
    var expandableBanner = $( '.tcu-expandablebanner-background' );

    // Hide items within the tcu-expandable-background
    expandableBanner.css( 'display', 'none' );

    // click event
    arrowButton.click( function() {

        // Toggle aria-expanded
        if ( 'false' === $( this ).attr( 'aria-expanded' ) ) {
            $( this )
                .attr( 'aria-expanded', 'true' )
                .html(
                    '<svg height="40" width="40"><use focusable="false" xlink:href="#up-arrow"></use></svg><span class="tcu-visuallyhidden">Contract Menu</span>'
                );
        } else {
            $( this )
                .attr( 'aria-expanded', 'false' )
                .html(
                    '<svg height="40" width="40"><use focusable="false" xlink:href="#down-arrow"></use></svg><span class="tcu-visuallyhidden">Expand Menu</span>'
                );
        }

        // Toggle slide
        expandableBanner.slideToggle();
    } );

    // Close menu when ESC key is pressed
    window.addEventListener( 'keydown', function( e ) {

        // If ESC key is pressed
        if ( 'Escape' === e.code && 27 === e.keyCode ) {
            arrowButton
                .attr( 'aria-expanded', 'false' )
                .attr( 'tabindex', '0' )
                .html(
                    '<svg height="40" width="40"><use focusable="false" xlink:href="#down-arrow"></use></svg><span class="tcu-visuallyhidden">Expand Menu</span>'
                );

            // Toggle slide
            expandableBanner.css( 'display', 'none' );
        }
    } );

    /*  IF YOU WANT TO USE DEVICE.JS TO DETECT THE VIEWPORT AND MANIPULATE THE OUTPUT  */

    // We grab our image size from the database
    var videoContainer = $( '.tcu-video-container' );

    // Only if tcu_image exists
    /* eslint-disable camelcase */
    var tcu_image = tcu_image ? tcu_image : '';

    if ( tcu_image ) {
        var mobileImg = tcu_image.background.sizes['tcu-700-550'];
        var mobileImageHeight = tcu_image.background.sizes['tcu-700-550-height'];
        var mobileImageWidth = tcu_image.background.sizes['tcu-700-550-width'];

        var tabletImg = tcu_image.background.sizes['tcu-1800-550'];
        var tabletImageHeight = tcu_image.background.sizes['tcu-1800-550-height'];
        var tabletImageWidth = tcu_image.background.sizes['tcu-1800-550-width'];
        /* eslint-enable camelcase */

        //Device.js will check if it is Tablet or Mobile - http://matthewhudson.me/projects/device.js/
        /* global device */
        if ( ! device.tablet() && ! device.mobile() ) {
            $( '#ytplayer' ).css( 'display', 'block' );
        } else if ( device.mobile() ) {
            videoContainer
                .removeClass( 'tcu-video-container' )
                .addClass( 'tcu-background-video-default' )
                .css( 'background-image', 'url(' + mobileImg + ')' )
                .css( 'height', mobileImageHeight )
                .css( 'width', mobileImageWidth );
            $( '#ytplayer' ).css( 'display', 'none' );
        } else if ( device.tablet() ) {
            videoContainer
                .removeClass( 'tcu-video-container' )
                .addClass( 'tcu-background-video-default' )
                .css( 'background-image', 'url(' + tabletImg + ')' )
                .css( 'height', tabletImageHeight )
                .css( 'width', tabletImageWidth );
            $( '#ytplayer' ).css( 'display', 'none' );
        }
    }

    $( '.tcu-frog-fountain-slider' ).slick( {
        dots: true,
        arrows: true,
        infinite: true,
        mobileFirst: true,
        adaptiveHeight: true,
        autoplay: true,
        autoplaySpeed: 6000,
        prevArrow:
            '<button type="button" data-role="none" class="slick-prev tcu-pulse" aria-label="Previous" tabindex="0" role="button">Previous</button>',
        nextArrow:
            '<button type="button" data-role="none" class="slick-next tcu-pulse" aria-label="Next" tabindex="0" role="button">Next</button>'
    } );
} ); /* end of as page load scripts */

( function() {
    var tcuMasonryHome = document.getElementById( 'tcu-masonry-home' );
    var tcuAlert = document.querySelector( '.tcu-modal--alert' );
    var tcuAlertClose = document.querySelector( '.tcu-close' );

    if ( tcuMasonryHome ) {
        /* global AnimOnScroll */
        var homeMasonryAnim = new AnimOnScroll( tcuMasonryHome, {
            minDuration: 0.4,
            maxDuration: 0.7,
            viewportFactor: 0.2
        } );

        homeMasonryAnim._init();
    }

    if ( tcuAlert && tcuAlertClose ) {
        tcuAlertClose.addEventListener( 'click', function() {
            tcuAlert.style.display = 'none';
            tcuAlert.setAttribute( 'aria-expanded', false );
        } );
    }
} () );
