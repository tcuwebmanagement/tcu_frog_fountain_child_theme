# TCU Frog Fountain Child Theme

The TCU Frog Fountain Child Theme is designed to incorporate TCU's Brand Standards. This theme contains must have the TCU's parent theme intalled.

**Our Files**

We offer two versions — a minified version, and an un-minified one. Use the minified version in a production environment or to reduce the file size of your downloaded assets. And the un-minified version is better if you are in a development environment or would like to debug the CSS or JavaScript assets in the browser.

**Our main SASS files are**

    library/scss

**WordPress Theme**

Run `grunt zip` to compress a clean zip version of the `dist` directory. From the WordPress admin screen, select Appearance > Themes from the menu. Click on Add new and select the zip file you just created to install. Finally, activate the theme.

    dist/

**jQuery**

WordPress comes with jQuery installed. Make sure to include jQuery if you are not using a WordPress theme.

**Working with an existing Grunt project**

If you haven't used Grunt before, be sure to check out the **[Getting Started Guide](http://gruntjs.com/getting-started)**, as it explains how to create a **[Gruntfile](http://gruntjs.com/sample-gruntfile)** as well as install and use Grunt plugins. Assuming that the `Grunt CLI` has been installed and that the project has already been configured with a package.json and a Gruntfile, it's very easy to start working with Grunt.

*   Change to the project's root directory `cd project/path/`
*   Install project dependencies with `npm install`.
*   Compress project with Grunt with `grunt zip`.

**Grunt Tasks**

`grunt build` to copy files to `dist` directory

`grunt zip` Compress the `dist` directory into `tcu_web_standards` folder and name the zip file `tcu_web_standards.VERSION.NUM.zip`

`npm run build` Runs the default grunt process, build, and zip all in one command.

**Install NPM**

**[Download NPM](https://www.npmjs.com/)**

Developed by **Mayra Perales**: <mailto:m.j.perales@tcu.edu>

## Special thanks to:

    - Eddie Machado - http://themble.com/bones
    - Paul Irish & the HTML5 Boilerplate
    - Yoast for some WP functions & optimization ideas
    - Andrew Rogers for code optimization
    - David Dellanave for speed & code optimization
    - and several other developers. :)

## Submit Bugs & or Fixes:

_[Create an issue](https://bitbucket.org/TCUWebmanage/tcu_frog_fountain_child_theme/issues?status=new&status=open)_

## Meta

*   [Changelog](CHANGELOG.md)
