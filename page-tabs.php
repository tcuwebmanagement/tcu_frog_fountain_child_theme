<?php
/**
 * Template Name: Tabs (with right sidebar)
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

get_header();

if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
	tcu_breadcrumbs_list();
}
?>

<div class="tcu-layoutwrap--transparent">

	<div class="tcu-layout-constrain cf">

		<main class="unit size2of3 m-size2of3 tcu-below32 cf" id="main">

		<?php
		if ( have_posts() ) :

			/**
			 * Start the loop.
			 */
			while ( have_posts() ) :
				the_post();
		?>

			<h1 id="post-<?php the_ID(); ?>"><?php the_title(); ?></h1>

			<article aria-labelledby="post-<?php the_ID(); ?>" <?php post_class( 'tcu-article cf' ); ?>>

				<?php if ( get_field( 'tabs_repeater' ) ) : ?>

			<!-- Responvise Tabs  -->
			<div class="tcu-responsive-tabs">

				<!-- Begin UL -->
				<ul>
					<?php
					/**
					 * Start the ACF loop.
					 */
					while ( have_rows( 'tabs_repeater' ) ) :
						the_row();

						/**
						 * ACF title variable
						 */
						$tcu_tabs_object = get_sub_field_object( 'tab_title' );
						$tcu_tabs_hash = hash('crc32b', $tcu_tabs_object['name']);
						$tcu_tabs_title = get_sub_field( 'tab_title' );
						$tcu_title      = preg_replace( '![^a-z0-9]+!i', '_', $tcu_tabs_title )  . '_' . $tcu_tabs_hash;

					?>

					<li><a href="#<?php echo esc_attr( strtolower( $tcu_title ) ); ?>"><?php the_sub_field( 'tab_title' ); ?></a></li>

					<?php
					/**
					 * End the ACF loop.
					 */
					endwhile;
					?>
				</ul><!-- End UL -->

				<?php
				/**
				 * Start the ACF loop.
				 */
				while ( have_rows( 'tabs_repeater' ) ) :
					the_row();

					/**
					 * ACF title variable
					 */
					$tcu_tabs_object = get_sub_field_object( 'tab_title' );
					$tcu_tabs_hash = hash('crc32b', $tcu_tabs_object['name']);
					$tcu_tabs_title = get_sub_field( 'tab_title' );
					$tcu_title      = preg_replace( '![^a-z0-9]+!i', '_', $tcu_tabs_title )  . '_' . $tcu_tabs_hash;

				?>

				<div class="tcu-article__content cf" id="<?php echo esc_attr( strtolower( $tcu_title ) ); ?>">
					<?php the_sub_field( 'tab_content' ); ?>
				</div>

				<?php
				/**
				 * End of the ACF loop.
				 */
				endwhile;
				?>

			</div><!-- end of .tcu-responsive-tabs -->

		<?php endif; ?>

			</article><!-- end of .tcu-article -->

			<?php

			/**
			 * End of the WP loop
			 */
			endwhile;

				else :

					// If no content, include the "No posts found" template.
					get_template_part( 'partials/content', 'none' );

		endif;
		?>

		</main><!-- end of .unit -->

		<?php get_sidebar(); ?>

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
