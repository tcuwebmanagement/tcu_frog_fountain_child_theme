<?php
/**
 * Template Name: Leadership Template
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

get_header();

if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
		tcu_breadcrumbs_list(); }
?>

		<div class="tcu-layoutwrap--transparent">

			<div class="tcu-layout-constrain cf">

				<main class="unit size1of1 m-size1of1 tcu-below16 cf" id="main">

					<?php
					/**
					 * Start the loop.
					 */
					if ( have_posts() ) :
						while ( have_posts() ) :
							the_post();

							get_template_part( 'partials/content', 'page' );

						/**
						 * End the loop.
						 */
						endwhile;

							else :

								get_template_part( 'partials/content', 'pagination' );

					endif;

							/**
							 * Begin ACF loop
							 * Profiles
							 */
							if ( have_rows( 'leadership_template_profile_section' ) ) :

									while ( have_rows( 'leadership_template_profile_section' ) ) :
										the_row();

										if ( 'leadership_template_profiles' === get_row_layout() ) :

											get_template_part( 'partials/content', 'leadership' );

										endif;

									endwhile;

							endif;

					?>

				</main><!-- end of .unit -->

			</div><!-- end of .tcu-layout-constrain -->

		</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
