/* global module */
module.exports = function( grunt ) {
    grunt.initConfig( {

        // Let's combine all our JS files into one
        concat: {

            // Front-end
            main: {
                src: [
                    'library/js/device.js',
                    'library/js/classie.js',
                    'library/js/imagesloaded.pkgd.js',
                    'library/js/AnimOnScroll.js',
                    'library/js/slick.js',
                    'library/js/tcu-frog-fountain-scripts.js'
                ],
                dest: 'library/js/concat/tcu-frog-fountain-scripts.js'
            }
        },

        // Let's minimize our JS files
        uglify: {

            // Front-end
            main: {
                src: 'library/js/concat/tcu-frog-fountain-scripts.js',
                dest: 'library/js/min/tcu-frog-fountain-scripts.min.js'
            }
        },

        //  Let's compile our SASS into CSS
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'library/css/style.css': 'library/scss/style.scss'
                }
            }
        },

        // Autoprefix and other polyfills
        postcss: {
            options: {
                map: true, // inline sourcemaps

                processors: [
                    require( 'autoprefixer' )( {
                        browsers: '> 2%, last 4 versions, Safari 8, IE 9, IE 8'
                    } )
                ]
            },
            dist: {
                src: 'library/css/style.css'
            }
        },

        // Let's provide our users a minified version of all our CSS files
        // This is done when you're ready to provide a final version copy
        cssmin: {
            target: {
                files: [
                    {
                        expand: true,
                        cwd: 'library/css/',
                        src: [ '*.css', '!*.min.css' ],
                        dest: 'library/css/',
                        ext: '.min.css'
                    }
                ]
            }
        },

        // Let's optimize our images and SVG files
        imagemin: {
            static: {
                options: {
                    optimizationLevel: 7,
                    svgoPlugins: [
                        {
                            removeViewBox: false,
                            removeAttrs: { attrs: [ 'xmlns' ] }
                        }
                    ]
                },
                files: {
                    './dist/library/images/svg/alert-triangle.svg':
                        './library/images/svg/alert-triangle.svg'
                }
            }
        },

        // This creates a clean WP theme copy to use in production
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: [
                            './*.php',
                            './*.css',
                            './CHANGELOG.md',
                            './README.md',
                            './*.png',
                            'partials/*.php',
                            'library/css/**.css',
                            'library/css/!*.map',
                            'library/js/**',
                            'library/inc/**.php',
                            'library/scss/**/*.scss',
                            'acf/**',
                            'wp-advanced-search/**'
                        ],
                        dest: 'dist/'
                    }
                ]
            }
        },

        // Let's zip up our /dist (production wordpress theme)
        // Change version number in style.css
        compress: {
            main: {
                options: {
                    archive: 'tcu_frog_fountain_child_theme.1.1.5.zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: 'dist/',
                        src: [ '**' ],
                        dest: 'tcu_frog_fountain_child_theme/'
                    }
                ]
            }
        },

        // Watch our files while we work
        watch: {
            css: {
                files: [ 'library/scss/**/*.scss' ],
                tasks: [ 'sass', 'newer:postcss' ]
            },
            cssmin: {
                files: [ 'library/css/style.css' ],
                tasks: [ 'cssmin' ],
                options: {
                    spawn: false
                }
            },
            scripts: {
                files: [
                    'library/js/device.js',
                    'library/js/slick.js',
                    'library/js/tcu-frog-fountain-scripts.js'
                ],
                tasks: [ 'concat', 'uglify' ]
            }
        },

        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'library/css/**.css',
                        'library/js/min/tcu-frog-fountain-scripts.min.js',
                        './**.php',
                        './partials/**.php'
                    ]
                },
                options: {
                    watchTask: true,
                    open: false, // Open project in a new tab?
                    injectChanges: true, // Auto inject changes instead of full reload.
                    proxy: 'localhost/~mjperales/wordpress/' // Use your localhost path to use BrowserSync.
                }
            }
        }
    } );

    grunt.loadNpmTasks( 'grunt-contrib-concat' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-imagemin' );
    grunt.loadNpmTasks( 'grunt-contrib-copy' );
    grunt.loadNpmTasks( 'grunt-contrib-compress' );
    grunt.loadNpmTasks( 'grunt-contrib-sass' );
    grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
    grunt.loadNpmTasks( 'grunt-postcss' );
    grunt.loadNpmTasks( 'grunt-contrib-watch' );
    grunt.loadNpmTasks( 'grunt-newer' );
    grunt.loadNpmTasks( 'grunt-browser-sync' );

    // optimize images
    grunt.registerTask( 'default', [
        'newer:concat',
        'newer:uglify',
        'newer:sass',
        'newer:cssmin',
        'newer:postcss'
    ] );

    // run browserSync and watch
    grunt.registerTask( 'dev', [ 'browserSync', 'watch' ] );

    // Build copy to production directory
    grunt.registerTask( 'build', [ 'copy' ] );

    // Zip our clean directory to easily install in WP
    grunt.registerTask( 'zip', [ 'compress' ] );
};
