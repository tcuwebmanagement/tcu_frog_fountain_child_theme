<?php
/**
 * Our core functions and hooks
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

/**
 * Enqueue parent style sheet
 * as well as the child style sheet
 */
function tcu_frog_fountain_child_theme_enqueue_styles() {

	$child_style = 'child-style';

	wp_enqueue_style(
		$child_style,
		/* Edit the path to your child themes stylesheet */
		get_stylesheet_directory_uri() . '/library/css/style.min.css',
		array( 'tcu-stylesheet' ), '1.1.5', false
	);

	// Make sure we only load our JS in the flexible content template.
	if ( is_page_template( 'page-flexible-content.php' ) ) {
		// Adding scripts file in the footer - this file includes ALL our JS files.
		wp_register_script( 'tcu-frog-fountain-scripts', get_stylesheet_directory_uri() . '/library/js/min/tcu-frog-fountain-scripts.min.js', array( 'jquery', 'jquery-masonry' ), '1.1.5', true );

		wp_enqueue_script( 'tcu-frog-fountain-scripts' );
	}
}

add_action( 'wp_enqueue_scripts', 'tcu_frog_fountain_child_theme_enqueue_styles' );

/**
 * INCLUDES
 */
require_once 'library/inc/tabs-acf-fields.php';
require_once 'library/inc/tcu-alert-acf.php';
require_once 'library/inc/tcu-alert.php';
require_once 'library/inc/tcu-flexible-content-fields.php';
require_once 'library/inc/tcu-profiles-fields.php';

/**
 * Check if the ACF plugin is intalled
 *
 * @return bool  True if the ACF plugin is installed
 */
function tcu_frog_fountain_child_theme_acf() {

	/* Checks to see if "is_plugin_active" function exists and if not load the php file that includes that function */
	if ( ! function_exists( 'is_plugin_active' ) ) {
		include_once ABSPATH . 'wp-admin/includes/plugin.php';
	}

	/* Checks to see if the acf pro plugin is activated  */
	if ( is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) || is_plugin_active( 'advanced-custom-fields/acf.php' ) ) {
		return true;
	} else {
		return false;
	}
}


/**
 * Admin notice to display when ACF is not installed
 */
function tcu_frog_fountain_child_theme_notice_missing_acf() {
	global $tcu_frog_fountain_notice_msg;

	$allowed_tags = array(
		'div' => array(
			'class' => array(),
		),
	);

	$tcu_frog_fountain_notice_msg = esc_html__( 'The TCU Frog Fountain Theme needs "Advanced Custom Fields PRO" plugin to run. Please download and activate it.', 'tcu_frog_fountain_child_theme' );

	if ( ! tcu_frog_fountain_child_theme_acf() ) {
		echo wp_kses( '<div class="notice notice-error notice-large"><div class="notice-title">' . $tcu_frog_fountain_notice_msg . '</div></div>', $allowed_tags );
	}
}

add_action( 'admin_notices', 'tcu_frog_fountain_child_theme_notice_missing_acf' );

/**
 * IMAGE SIZES
 */
add_image_size( 'tcu-200-150', 200, 150, true );
add_image_size( 'tcu-550-200', 550, 200, true );
add_image_size( 'tcu-480-550', 480, 550, true );
add_image_size( 'tcu-700-550', 700, 550, true );
add_image_size( 'tcu-1000-550', 1000, 550, true );
add_image_size( 'tcu-1800-550', 1800, 550, true );

/**
 * Hide ACF except for Super Admins and Admins
 *
 * @param string $show The current capabilities for the user.
 */
function tcu_acf_show_admin( $show ) {

	$show = current_user_can( 'manage_options' );
	return $show;
}

add_filter( 'acf/settings/show_admin', 'tcu_acf_show_admin' );

/**
 * TCU News Custom Post Type
 * Select keywords in our flexible content page template
 *
 * @param  array $field The field settings array.
 * @return array  $field  Modified settings array.
 */
function tcu_acf_load_news_keyword_choices( $field ) {

	// Bail if TCU News is not installed.
	if ( ! function_exists( 'tcu_news_query' ) ) {
		return;
	}

	// Reset choices.
	$field['choices'] = array();

	// Get all keywords.
	$keywords = get_terms(
		array(
			'taxonomy'   => 'tcu_news_keyword',
			'hide_empty' => false,
		)
	);

	// Make sure we have the default selection.
	$new_choices = array( 'View all news ( tcu_all_news )' );

	foreach ( $keywords as $key ) {
		array_push( $new_choices, $key->name . ' (' . $key->slug . ')' );
	}

	if ( is_array( $new_choices ) ) {

		foreach ( $new_choices as $key ) {

			$field['choices'][ $key ] = $key;

		}
	} else {
		$field['choices'][ $new_choices ] = $new_choices;
	}

	// Return the field.
	return $field;

}

add_filter( 'acf/load_field/name=tcu_news_section_select_keyword', 'tcu_acf_load_news_keyword_choices' );


