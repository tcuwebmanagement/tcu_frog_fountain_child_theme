<?php
/**
 * Template part to display Highlight box section (Three Columns)
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

if ( have_rows( 'highlight_sections_three_columns_hightlight_section_repeat' ) ) : ?>

<div class="tcu-layoutwrap--grey tcu-pad-tb0 tcu-pad-lr0 cf">

	<div class="tcu-layout--large tcu-layout-center cf">

	<?php
	/**
	 * Start the ACF loop.
	 */
	while ( have_rows( 'highlight_sections_three_columns_hightlight_section_repeat' ) ) :
		the_row();

		// ACF Variables.
		$tcu_image      = get_sub_field( 'highlight_sections_three_columns_image' );
		$tcu_title      = get_sub_field( 'highlight_sections_three_columns_title' );
		$tcu_link_text  = get_sub_field( 'highlight_sections_three_columns_link_text' );
		$tcu_link       = get_sub_field( 'highlight_sections_three_columns_link' );
		$tcu_aria_label = get_sub_field( 'highlight_sections_three_columns_aria-label' );

	/**
	 * We inlined our styles in order to change the image size for each media query
	 * Faster loading time and smaller images for mobile
	 * Performance is important!
	 */
	if ( ! empty( $tcu_image ) ) :
	?>
	<style>
	/* <![CDATA[ */
	#tcu-highlight-<?php echo sanitize_html_class( $tcu_image['name'] ); ?> {
		background: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url('<?php echo esc_url( $tcu_image['sizes']['tcu-480-550'] ); ?>') center center no-repeat;
	}
	/* ]]> */
	</style>
<?php endif; ?>
	<div id="tcu-highlight-<?php echo esc_attr( $tcu_image['name'] ); ?>" class="unit size1of3 m-size1of1 tcu-background-defaults">

		<div class="tcu-zindex-2 tcu-absolute-btm-left">

			<?php if ( $tcu_title ) : ?>
				<h3 class="tcu-uppercase h2"><?php echo esc_html( $tcu_title ); ?></h3>
			<?php endif; ?>

			<?php if ( $tcu_link && $tcu_link_text && $tcu_aria_label ) : ?>

				<a aria-label="<?php echo esc_attr( $tcu_aria_label ); ?>" class="tcu-button tcu-button--primary tcu-bounce tcu-bounce--right--grey" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_link_text ); ?></a>

			<?php elseif ( $tcu_link && $tcu_link_text ) : ?>

				<a class="tcu-button tcu-button--primary tcu-bounce tcu-bounce--right--grey" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_link_text ); ?></a>

			<?php endif; ?>

		</div><!-- end of .tcu-zindex-2 -->

	</div><!-- end of .tcu-background-defaults -->

	<?php
	/**
	 * End of the ACF loop.
	 */
	endwhile;
	?>
	</div>

</div><!-- end of .tcu-layoutwrap--grey -->
<?php endif; ?>
