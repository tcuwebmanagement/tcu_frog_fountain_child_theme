<?php
/**
 * Template part to display the slider
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

// check if the flexible content field has rows of data & loop.
if ( have_rows( 'large_slider_section_repeater' ) ) :
?>

<div class="tcu-layoutwrap--purple tcu-pad-tb0 tcu-pad-lr0">

	<div class="tcu-layout--large tcu-layout-center">

		<section class="tcu-frog-fountain-slider">

			<?php
			/**
			 * Start the ACF loop.
			 */
			while ( have_rows( 'large_slider_section_repeater' ) ) :
				the_row();

				// ACF Variables.
				$tcu_image        = get_sub_field( 'large_slider_section_background_image' );
				$tcu_title        = get_sub_field( 'large_slider_section_title' );
				$tcu_content      = get_sub_field( 'large_slider_section_content' );
				$tcu_link_text    = get_sub_field( 'large_slider_section_link_text' );
				$tcu_link         = get_sub_field( 'large_slider_section_link' );
				$tcu_aria_label   = get_sub_field( 'large_slider_section_aria-label' );
				$tcu_display_copy = get_sub_field( 'slider_section_display_purple_background_with_copy' );
			?>

			<div class="tcu-frog-fountain-slider-body">

				<div class="tcu-frog-slider-image" style="background-image: url('<?php echo esc_url( $tcu_image['url'] ); ?>'); background-size: cover;"></div><!-- end of .frog-fountain-slider-image -->

				<?php if ( $tcu_display_copy ) : ?>
				<div class="tcu-frog-fountain-slider-content tcu-overlay--purple">

					<?php if ( $tcu_title ) : ?>
					<h3 class="tcu-mar-t0 tcu-arvo tcu-font-bold h2"><?php echo esc_html( $tcu_title ); ?></h3>
					<?php endif; ?>

					<?php
					if ( $tcu_content ) :
						echo wp_kses_post( $tcu_content );
					endif;
					?>

					<?php if ( $tcu_aria_label && $tcu_link && $tcu_link_text ) : ?>
						<!-- Read More Button -->
						<div class="tcu-layout-center tcu-alignc tcu-full-width tcu-top32">
							<a aria-label="<?php echo esc_attr( $tcu_aria_label ); ?>" class="tcu-button tcu-button--secondary tcu-bounce tcu-bounce--right--grey tcu-alignc" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_link_text ); ?></a>
						</div>
					<?php elseif ( $tcu_link && $tcu_link_text ) : ?>

						<!-- Read More Button -->
						<div class="tcu-layout-center tcu-alignc tcu-full-width tcu-top32">
							<a class="tcu-button tcu-button--secondary tcu-bounce tcu-bounce--right--grey tcu-alignc" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_link_text ); ?></a>
						</div>

					<?php endif; ?>

				</div><!-- .tcu-frog-fountain-slider-content -->
				<?php endif; ?>

			</div><!-- end of .tcu-frog-foutain-slider-body -->

			<?php
			/**
			 * End the ACF loop.
			 */
			endwhile;
			?>

		</section><!-- end of .tcu-frog-fountain-slider -->

	</div><!-- end of .tcu-layout--large -->

</div><!-- end of .tcu-layoutwrap--purple -->

<?php endif; ?>
