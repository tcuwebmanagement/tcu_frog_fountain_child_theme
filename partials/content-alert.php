<?php
/**
 * Template part to display the TCU Alert section
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

?>
<!-- TCU Alert -->
<div class="tcu-layoutwrap--yellow tcu-modal--alert cf">

	<button title="Close TCU Alert Message" class="tcu-close" type="button">X<span class="tcu-visuallyhidden">Close Window</span></button>

	<div class="tcu-layout-constrain tcu-modal__wrapper">

		<h2 class="tcu-arvo tcu-alignc tcu-uppercase tcu-font-bold"><?php esc_html( the_field( 'tcu_alert_title', 'option' ) ); ?></h2>

		<div class="group unit size1of1 m-size1of1 tcu-below32 cf">

			<div class="unit size1of8 m-size1of4 tcu-alert-icon tcu-flexbox tcu-flexbox--vertical-align cf">
				<svg width="65" height="58">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g transform="translate(-147.000000, -120.000000)">
							<rect fill="#F9D44B" x="0" y="0" width="1024" height="305"></rect>
							<polygon fill="#D0021B" points="179.5 120 212 178 147 178"></polygon>
							<text font-family="Arvo-Bold, Arvo" font-size="40" font-weight="bold" fill="#FFFFFF">
								<tspan x="174" y="171">!</tspan>
							</text>
						</g>
					</g>
				</svg>
			</div>

			<div class="unit size7of8 m-size3of4 tcu-article__content">

				<?php wp_kses_post( the_field( 'tcu_alert_message', 'option' ) ); ?>

			</div>

		</div>

		<div class="tcu-layout-center tcu-alignc tcu-below16 cf">

			<a class="tcu-button tcu-button--secondary tcu-bounce tcu-bounce--right--grey tcu-alignc" href="<?php esc_url( the_field( 'tcu_alert_link', 'option' ) ); ?>"><?php esc_html_e( 'Latest Information', 'tcu_frog_fountain_child_theme' ); ?></a>

		</div>
	</div>
</div><!-- end of .tcu-layoutwrap--yellow -->
