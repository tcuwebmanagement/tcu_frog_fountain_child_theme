<?php
/**
 * Template part to display a news feed
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

// ACF variables.
$tcu_title     = get_sub_field( 'tcu_news_section_title' );
$tcu_date      = get_sub_field( 'tcu_news_section_display_date' );
$tcu_images    = get_sub_field( 'tcu_news_section_display_images' );
$tcu_excerpt   = get_sub_field( 'tcu_news_section_display_excerpt' );
$tcu_keywords  = get_sub_field( 'tcu_news_section_display_keywords' );
$tcu_num_posts = get_sub_field( 'tcu_news_section_number_of_posts_to_show' );
$tcu_terms     = get_sub_field( 'tcu_news_section_select_keyword' );
$tcu_link      = get_sub_field( 'tcu_news_section_link' );
$tcu_slug      = ( preg_match( '#\((.*?)\)#', $tcu_terms, $match ) ) ? $match[1] : '';


// Set up our query.
if ( function_exists( 'tcu_news_query' ) ) {
	$tcu_posts = ( trim( $tcu_slug ) === 'tcu_all_news' ) ? tcu_news_query( $tcu_num_posts ) : tcu_news_query( $tcu_num_posts, $tcu_slug );
}

if ( count( $tcu_posts ) ) : ?>
<div class="tcu-layoutwrap--transparent cf">

	<div class="tcu-layout-constrain cf">

	<?php if ( $tcu_title ) : ?>
		<h4 class="tcu-mar-t0 tcu-mar-t0 tcu-arvo tcu-alignc tcu-font-bold tcu-border--purple h2"><?php echo esc_html( $tcu_title ); ?></h4>
	<?php endif; ?>

	<div class="tcu-masonry-home effect-2 cf" id="tcu-masonry-home">

	<?php
	foreach ( $tcu_posts as $post ) :
		setup_postdata( $post );
	?>

		<article class="tcu-article tcu-modal cf" role="article">

			<?php
			if ( $tcu_images ) {
				if ( has_post_thumbnail() ) {
					the_post_thumbnail( 'tcu_news_thumb' );
				}
			}
			?>

			<section class="tcu-modal__content">

				<h5 class="tcu-arvo tcu-mar-b0 h4"><?php the_title(); ?></h5>

				<?php if ( $tcu_date ) : ?>
					<p class="tcu-byline">
						<span><time class="updated entry-time" datetime="<?php echo esc_attr( get_the_time( 'Y-m-d' ) ); ?>" itemprop="datePublished"><?php echo esc_html( get_the_time( get_option( 'date_format' ) ) ); ?></time></span>
					</p>
				<?php endif; ?>

				<?php
				if ( $tcu_excerpt ) :
					$tcu_excerpt = substr( get_the_excerpt(), 0, 150 );
					echo esc_html( $tcu_excerpt . '...' );
				endif;
				?>

				<?php if ( $tcu_keywords ) : ?>
					<p><?php echo get_the_term_list( $post->ID, 'tcu_news_keyword', '', ' / ', '' ); ?></p>
				<?php endif; ?>

			</section><!-- end of tcu-modal__content -->

			<!-- Our Read More button -->
			<a aria-label="<?php esc_html_e( 'Read more about ', 'tcu_frog_fountain_child_theme' ) . the_title(); ?>" title="<?php the_title_attribute(); ?>" class="tcu-button tcu-button--primary tcu-bounce tcu-bounce--right--grey tcu-full-width" href="<?php the_permalink(); ?>">Read More<svg height="30" width="30"><use focusable="false" xlink:href="#play-icon"></use></svg></a>

		</article><!-- end of .tcu-article -->

	<?php
	endforeach;
	wp_reset_postdata();
	?>

	</div><!-- end of .tcu-masonry -->

	<?php endif; ?>

	<div class="tcu-layout-center tcu-alignc tcu-top32 tcu-below32"><!-- Our Read More button -->
		<a title="Read More News" class="tcu-button tcu-button--primary tcu-bounce tcu-bounce--right--grey tcu-alignc" href="<?php echo esc_url( $tcu_link ); ?>">More News</a>
	</div>

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end of .tcu-layoutwrap--transparent -->
