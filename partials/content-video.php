<?php
/**
 * Template part to display video section
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

// Variables.
$tcu_video        = get_sub_field( 'large_video_section_video' );
$tcu_title        = get_sub_field( 'large_video_section_title' );
$tcu_content      = get_sub_field( 'large_video_section_content' );
$tcu_link_text    = get_sub_field( 'large_hero_image_section_link_text' );
$tcu_link         = get_sub_field( 'large_video_section_link' );
$tcu_aria_label   = get_sub_field( 'large_hero_image_section_aria-label' );
$tcu_display_copy = get_sub_field( 'display_purple_background_with_copy' );
$tcu_image        = get_sub_field( 'large_video_section_image_for_video' );
$tcu_src          = ( preg_match( '/src="(.+?)"/', $tcu_video, $matches ) ) ? $matches[1] : '';
$tcu_allowed      = array(
	'iframe' => array(
		'id'              => array(),
		'src'             => array(),
		'allow'           => array(),
		'encrypted-media' => array(),
		'allowfullscreen' => array(),
		'tabindex'        => array(),
		'style'           => array(),
		'frameborder'     => array(),
		'height'          => array(),
		'width'           => array(),
	),
);

// Let's pass our $image URL to our JS file.
$tcu_image_array = array(
	'background' => $tcu_image,
);
wp_localize_script( 'tcu-frog-fountain-scripts', 'tcu_image', $tcu_image_array );

// Find the video ID.
preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $tcu_src, $match );

// Add extra params to iframe src.
$tcu_params = array(
	'controls'    => 0,
	'showinfo'    => 0,
	'autoplay'    => 1,
	'loop'        => 1,
	'rel'         => 0,
	'hd'          => 1,
	'enablejsapi' => 1,
	'autohide'    => 1,
	'playlist'    => $match[1],
);

// Rebuild our URL.
$tcu_new_src = add_query_arg( $tcu_params, $tcu_src );

// Add extra params to iframe src.
$tcu_video = str_replace( $tcu_src, $tcu_new_src, $tcu_video );

// Add extra attributes to iframe html.
$tcu_attributes = 'frameborder="0" tabindex="-1" id="ytplayer"';

$tcu_video = str_replace( '></iframe>', ' ' . $tcu_attributes . '></iframe>', $tcu_video );

// Mute our video with the script below.
?>
<script>
	var tag = document.createElement('script');
	tag.src = "//www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	function onYouTubePlayerAPIReady() {

		player = new YT.Player('ytplayer', {
			events: {
				'onReady': function(e) {
					e.target.mute();
				}
			}
		});
	}
</script>

<!-- Large Video  -->
<div class="tcu-layoutwrap--purple tcu-pad-tb0 tcu-pad-lr0">
	<div class="tcu-video cf">

		<div class="tcu-video-container">
			<?php
			if ( $tcu_video ) {
				echo wp_kses( $tcu_video, $tcu_allowed ); }
			?>
		</div>

		<?php if ( $tcu_display_copy ) : ?>

			<div class="tcu-hero__content tcu-overlay--purple tcu-layout-center">

				<?php if ( $tcu_title ) : ?>
					<h2 class="tcu-arvo tcu-font-bold h1 tcu-mar-t0"><?php echo esc_html( $tcu_title ); ?></h2>
				<?php
				endif;

				/**
				 * If textarea is not empty
				 * Automatically adds paragraphs
				 */
				if ( $tcu_content ) :
					echo wp_kses_post( $tcu_content );
				endif;

				if ( $tcu_aria_label && $tcu_link && $tcu_link_text ) :
				?>

					<a aria-label="<?php echo esc_attr( $tcu_aria_label ); ?>" class="tcu-button tcu-button--secondary tcu-bounce tcu-bounce--right--grey tcu-top16" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_link_text ); ?></a>

				<?php elseif ( $tcu_link && $tcu_link_text ) : ?>

					<a class="tcu-button tcu-button--secondary tcu-bounce tcu-bounce--right--grey tcu-top16" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_link_text ); ?></a>

				<?php endif; ?>

			</div><!-- end of .tcu-hero__content -->

		<?php endif; ?>
	</div><!-- end of .tcu-video -->

</div><!-- end of tcu-layoutwrap--purple -->
