<?php
/**
 * Template part to display the hero image section
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

// Main Content.
$tcu_main_title   = get_sub_field( 'six_block_section_title' );
$tcu_main_content = get_sub_field( 'six_block_section_content' );

// Top Left Button with image.
$tcu_top_left_image      = get_sub_field( 'six_block_section_top_left_image' );
$tcu_top_left_title      = get_sub_field( 'six_block_section_top_left_image_link_text' );
$tcu_top_left_link       = get_sub_field( 'six_block_section_top_left_image_link' );
$tcu_top_left_aria_label = get_sub_field( 'six_block_section_top_left_image_aria_label' );

// Bottom Left button (Blue).
$tcu_btm_left_blue_title      = get_sub_field( 'six_block_section_title_blue_box_link_text' );
$tcu_btm_left_blue_link       = get_sub_field( 'six_block_section_link_blue_box_btm_left' );
$tcu_btm_left_blue_aria_label = get_sub_field( 'six_block_section_title_blue_box_aria_label' );

// Bottom Left button (Tan).
$tcu_btm_left_tan_title      = get_sub_field( 'six_block_section_link_tan_text_btm_left' );
$tcu_btm_left_tan_link       = get_sub_field( 'six_block_section_link_tan_box_btm_left' );
$tcu_btm_left_tan_aria_label = get_sub_field( 'six_block_section_btm_tan_box_aria_label' );

// Top Left button (blue).
$tcu_top_left_blue_title      = get_sub_field( 'six_block_section_link_text_blue_box_top_left' );
$tcu_top_left_blue_link       = get_sub_field( 'six_block_section_link_blue_box_top_left' );
$tcu_top_left_blue_aria_label = get_sub_field( 'six_block_section_aria_label_text_blue_box_top_left' );

// Top Right button (Tan).
$tcu_top_right_tan_title      = get_sub_field( 'six_block_section_title_tan_box_top_right' );
$tcu_top_right_tan_link       = get_sub_field( 'six_block_section_link_tan_box_top_right' );
$tcu_top_right_tan_aria_label = get_sub_field( 'six_block_section_aria_label_tan_button' );

// Bottom Right Button with image.
$tcu_btm_right_image      = get_sub_field( 'six_block_section_btm_right_image' );
$tcu_btm_right_title      = get_sub_field( 'six_block_section_btm_right_link_text' );
$tcu_btm_right_link       = get_sub_field( 'six_block_section_btm_right_link' );
$tcu_btm_right_aria_label = get_sub_field( 'six_block_section_btm_right_aria_label' );
?>

<!-- SVG for top angles -->
<div class="tcu-background--angles"></div>

<div class="tcu-layoutwrap--purple tcu-position--relative tcu-background--triangles">

	<?php // Sometimes it's better to inline styles. ?>
	<div style="padding-bottom: 100px;" class="tcu-layout-constrain tcu-position--relative cf">

		<h3 class="tcu-arvo tcu-font-bold h2"><?php echo esc_html( $tcu_main_title ); ?></h3>

		<?php
		/**
		 * Top content
		 */
		echo wp_kses_post( $tcu_main_content );
		?>

		<div class="group size1of1 m-size1of1 tcu-top32">

			<div class="group unit size1of2 m-size1of2">

				<div style="background: transparent url('<?php echo esc_url( $tcu_top_left_image['sizes']['tcu-550-200'] ); ?>') top center no-repeat; background-size: cover; height: 200px;" class="unit size1of1 m-size1of1 tcu-flexbox tcu-flexbox--vertical-align tcu-flexbox--align-items tcu-button--image">

					<div class="tcu-overlay-defaults tcu-overlay--purple"></div>

					<a href="<?php echo esc_url( $tcu_top_left_link ); ?>" class="tcu-button tcu-button--primary"><?php echo esc_html( $tcu_top_left_title ); ?></a>

				</div><!-- end of .tcu-button--image -->

				<div class="group unit size1of1 m-size1of1">

					<a href="<?php echo esc_url( $tcu_btm_left_blue_link ); ?>" class="unit size1of2 m-size1of2 tcu-flexbox tcu-flexbox--vertical-align tcu-flexbox--align-items tcu-button--square tcu-button--square--blue"><?php echo esc_html( $tcu_btm_left_blue_title ); ?></a>

					<a href="<?php echo esc_url( $tcu_btm_left_tan_link ); ?>" class="unit size1of2 m-size1of2 tcu-flexbox tcu-flexbox--vertical-align tcu-flexbox--align-items tcu-button--square tcu-button--square--tan"><?php echo esc_html( $tcu_btm_left_tan_title ); ?></a>

				</div><!-- end of .group.size1of1 -->

			</div><!-- end of .group.size1of2 -->

			<div class="group unit size1of2 m-size1of2">

				<div class="group unit size1of1 m-size1of1">

					<a href="<?php echo esc_url( $tcu_top_left_blue_link ); ?>" class="unit size1of2 m-size1of2 tcu-flexbox tcu-flexbox--vertical-align tcu-flexbox--align-items tcu-button--square tcu-button--square--blue"><?php echo esc_html( $tcu_top_left_blue_title ); ?></a>

					<a href="<?php echo esc_url( $tcu_top_right_tan_link ); ?>" class="unit size1of2 m-size1of2 tcu-flexbox tcu-flexbox--vertical-align tcu-flexbox--align-items tcu-button--square tcu-button--square--tan"><?php echo esc_html( $tcu_top_right_tan_title ); ?></a>

				</div><!-- end of .group.size1of1 -->

				<div style="background: transparent url('<?php echo esc_url( $tcu_btm_right_image['sizes']['tcu-550-200'] ); ?>') top center no-repeat; background-size: cover; height: 200px;" class="unit size1of1 m-size1of1 tcu-flexbox tcu-flexbox--vertical-align tcu-flexbox--align-items tcu-button--image">

					<div class="tcu-overlay-defaults tcu-overlay--purple"></div>
					<a href="<?php echo esc_url( $tcu_btm_right_link ); ?>" class="tcu-button tcu-button--primary"><?php echo esc_html( $tcu_btm_right_title ); ?></a>

				</div><!-- end of .tcu-button--image -->

			</div><!-- end of .group.size1of2 -->

		</div><!-- end of .group.size1of1 -->

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end of .tcu-layoutwrap--purple -->
