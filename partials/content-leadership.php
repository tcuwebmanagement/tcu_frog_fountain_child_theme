<?php
/**
 * Template part to display leadership profiles
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

?>

<section class="tcu-leadership-wrapper">

	<h2 class="tcu-arvo tcu-font-bold"><?php the_sub_field( 'leadership_template_profiles_title' ); ?></h2>

	<?php the_sub_field( 'leadership_template_profiles_content' ); ?>

	<div class="tcu-flexbox tcu-flexbox--row-wrap tcu-flex-space-around tcu-flexbox--vertical-align tcu-top32">

		<?php

		if ( have_rows( 'leadership_template_profiles_profiles_repeater' ) ) :

			/**
			 * Start repeater loop
			 */
			while ( have_rows( 'leadership_template_profiles_profiles_repeater' ) ) :
				the_row();

				// ACF image variable.
				$tcu_image = get_sub_field( 'leadership_template_profiles_image' );
		?>

		<div class="tcu-leadership-profile tcu-flex-start">

			<img height="150" width="200" src="<?php echo esc_url( $tcu_image['sizes']['tcu-200-150'] ); ?>" alt="<?php echo esc_attr( $tcu_image['alt'] ); ?>">

			<p class="tcu-mar-t0">
				<?php if ( get_sub_field( 'leadership_template_profiles_link' ) ) : ?>
					<a href="<?php esc_url( the_sub_field( 'leadership_template_profiles_link' ) ); ?>"><?php esc_html( the_sub_field( 'leadership_template_profiles_full_name' ) ); ?></a><br>
				<?php else : ?>
					<span><?php esc_html( the_sub_field( 'leadership_template_profiles_full_name' ) ); ?></span><br>
				<?php endif; ?>

				<?php if ( get_sub_field( 'leadership_template_profiles_title' ) ) : ?>
					<?php esc_html( the_sub_field( 'leadership_template_profiles_title' ) ); ?> <br>
				<?php endif; ?>


				<?php if ( get_sub_field( 'leadership_template_profiles_email' ) ) : ?>
					<a href="mailto:<?php esc_attr( the_sub_field( 'leadership_template_profiles_email' ) ); ?>"><?php esc_html( the_sub_field( 'leadership_template_profiles_email' ) ); ?></a> <br />
				<?php endif; ?>

				<?php if ( get_sub_field( 'leadership_template_profiles_phone_number' ) ) : ?>
					<span><?php esc_html( the_sub_field( 'leadership_template_profiles_phone_number' ) ); ?></span>
				<?php endif; ?>
			</p>

		</div><!-- end of .tcu-leadership-profile -->

		<?php
		/**
		 * End the ACF loop
		 */
		endwhile;

	endif;
	?>

	</div><!-- end of .tcu-flexbox -->

</section><!-- end of .tcu-leadership-wrapper -->
