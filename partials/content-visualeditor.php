<?php
/**
 * Template part to display full width visual editor
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

?>
<div class="tcu-layoutwrap tcu-background--dots">

	<div class="tcu-layout-constrain inner-content cf">

		<div class="unit size1of1 m-size1of1 tcu-below16 cf">

			<section class="tcu-article__content cf" role="region">

				<?php the_sub_field( 'frog_fountain_visual_editor_content' ); ?>

			</section>

		</div><!-- end of .unit -->

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end .tcu-layoutwrap--transparent -->
