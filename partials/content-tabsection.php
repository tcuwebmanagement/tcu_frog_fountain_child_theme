<?php
/**
 * Template part to display tab section
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

// ACF Variables.
$tcu_title   = get_sub_field( 'tabs_section_title' );
$tcu_content = get_sub_field( 'tabs_section_content' );
?>

<div class="tcu-layoutwrap--transparent tcu-below32 cf">

	<div class="tcu-layout-constrain tcu-layout-center cf">

		<div class="unit size1of1 m-size1of1">

			<div class="tcu-article__content tcu-below32">

				<?php if ( $tcu_title ) : ?>

					<h3 class="tcu-uppercase tcu-arvo tcu-font-bold h2"><?php echo esc_html( $tcu_title ); ?></h3>

				<?php
				endif;

				if ( $tcu_content ) :
					echo wp_kses_post( $tcu_content );
				endif;

				?>
			</div><!-- end of .tcu-article__content -->

			<?php
			// Check if the flexible content field has rows of data & loop through rows.
			if ( have_rows( 'tabs_section_tabs_repeater' ) ) :
			?>

				<!-- Start our Tabs -->
				<div class="tcu-responsive-tabs">

					<ul>
						<?php
						/**
						 * Start the ACF loop.
						 */
						while ( have_rows( 'tabs_section_tabs_repeater' ) ) :

							the_row();

							// ACF Variables.
							$tcu_tabs_object = get_sub_field_object( 'tabs_section_tabs_repeater_title' );
							$tcu_tabs_hash = hash('crc32b', $tcu_tabs_object['name']);
							$tcu_tabs_title = get_sub_field( 'tabs_section_tabs_repeater_title' )  . '-' . $tcu_tabs_hash;
						?>

						<li>
							<a href="#<?php echo sanitize_html_class( $tcu_tabs_title ); ?>"><?php sanitize_text_field( the_sub_field( 'tabs_section_tabs_repeater_title' ) ); ?></a>
						</li>

						<?php
						/**
						 * End the ACF loop.
						 */
						endwhile;
						?>
					</ul>

				<?php

				/**
				 * Start the ACF loop.
				 */
				while ( have_rows( 'tabs_section_tabs_repeater' ) ) :

					the_row();

					// ACF Variables.
					$tcu_tabs_object = get_sub_field_object( 'tabs_section_tabs_repeater_title' );
					$tcu_tabs_hash = hash('crc32b', $tcu_tabs_object['name']);
					$tcu_tabs_title = get_sub_field( 'tabs_section_tabs_repeater_title' )  . '-' . $tcu_tabs_hash;
				?>

					<!-- Start Content Section -->
					<div id="<?php echo sanitize_html_class( $tcu_tabs_title ); ?>" class="cf"><?php wp_kses_post( the_sub_field( 'tabs_section_tabs_repeater_content' ) ); ?></div>

				<?php
				/**
				 * End the ACF loop.
				 */
				endwhile;
				?>

				</div><!-- end of .responsive-tabs -->

			<?php endif; ?>

		</div><!-- end of .size1of2 -->

	</div><!-- end of .tcu-layout--large -->

</div><!-- end of .tcu-layoutwrap--grey -->
