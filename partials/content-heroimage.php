<?php
/**
 * Template part to display the hero image section
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

// ACF Variables.
$tcu_image           = get_sub_field( 'large_hero_image_section_image' );
$tcu_title           = get_sub_field( 'large_hero_image_section_title' );
$tcu_content         = get_sub_field( 'large_hero_image_section_content' );
$tcu_link_text       = get_sub_field( 'large_hero_image_section_link_text' );
$tcu_link            = get_sub_field( 'large_hero_image_section_link' );
$tcu_aria_label      = get_sub_field( 'large_hero_image_section_aria-label' );
$tcu_display_content = get_sub_field( 'display_purple_background_with_copy' );

/**
 * We inlined our styles in order to change the image size for each Media Query
 * Faster loading time and smaller images for mobile
 * Performance is important!
 */

if ( ! empty( $tcu_image ) ) : ?>
<style>
	/* <![CDATA[ */
	.tcu-hero {
		background: url('<?php echo esc_url( $tcu_image['sizes']['tcu-480-550'] ); ?>') center center no-repeat;
		background-size: cover;
	}

	@media screen and ( min-width: 481px ) {
		.tcu-hero {
			background: url('<?php echo esc_url( $tcu_image['sizes']['tcu-1000-550'] ); ?>') center center no-repeat;
			background-size: cover;
		}
	}

	@media screen and ( min-width: 1000px ) {
		.tcu-hero {
			background: url('<?php echo esc_url( $tcu_image['sizes']['tcu-1800-550'] ); ?>') center center no-repeat;
			background-size: cover;
		}
	}

	@media screen and ( min-width: 1200px ) {
		.tcu-hero {
			background: url('<?php echo esc_url( $tcu_image['url'] ); ?>') center center no-repeat;
			background-size: cover;
		}
	}
	/* ]]> */
</style>
<?php endif; ?>

<!-- Hero Image -->
<div class="tcu-layoutwrap--purple tcu-pad-tb0 tcu-pad-lr0">

	<div class="tcu-hero cf">

		<?php if ( $tcu_display_content ) : ?>

			<div class="tcu-hero__content tcu-overlay--purple tcu-layout-center">

				<?php if ( $tcu_title ) : ?>
					<h2 class="tcu-arvo tcu-font-bold h1 tcu-mar-tb0"><?php echo esc_html( $tcu_title ); ?></h2>
				<?php endif; ?>

				<?php
				/**
				 * If textarea is not empty
				 * Automatically adds paragraphs
				 */
				if ( $tcu_content ) :
					echo wp_kses_post( $tcu_content );
				endif;

				if ( $tcu_aria_label && $tcu_link && $tcu_link_text ) :
				?>

					<a aria-label="<?php echo esc_attr( $tcu_aria_label ); ?>" class="tcu-button tcu-button--secondary tcu-bounce tcu-bounce--right--grey tcu-top16" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_link_text ); ?></a>

						<?php elseif ( $tcu_link && $tcu_link_text ) : ?>

					<a class="tcu-button tcu-button--secondary tcu-bounce tcu-bounce--right--grey tcu-top16" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_link_text ); ?></a>

				<?php endif; ?>

			</div><!-- end of .tcu-background-skew-purple -->

	<?php endif; ?>

	</div><!-- end of .tcu-hero -->

</div><!-- end of tcu-layoutwrap--purple -->
