<?php
/**
 * Template part to display the inforgraphics
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

// ACF Variables.
$tcu_image = get_sub_field( 'infographics_section_background_image' );

if ( ! empty( $tcu_image ) ) : ?>
<style>
	/* <![CDATA[ */
	.tcu-background-infographics {
		background: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url('<?php echo esc_url( $tcu_image['sizes']['tcu-480-550'] ); ?>') center center no-repeat;
		background-size: cover;
	}

	@media screen and ( min-width: 481px ) {
		.tcu-background-infographics {
		background: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url('<?php echo esc_url( $tcu_image['sizes']['tcu-1000-550'] ); ?>') center center no-repeat;
		background-size: cover;
		}
	}

	@media screen and ( min-width: 1000px ) {
	.tcu-background-infographics {
		background: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url('<?php echo esc_url( $tcu_image['sizes']['tcu-1800-550'] ); ?>') center center no-repeat;
		background-size: cover;
		}
	}

	@media screen and ( min-width: 1200px ) {
	.tcu-background-infographics {
		background: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url('<?php echo esc_url( $tcu_image['url'] ); ?>') center center no-repeat;
		background-size: cover;
		}
	}
	/* ]]> */
</style>
<?php endif; ?>


<div class="tcu-layout--large tcu-layout-center cf">

	<div class="tcu-background-infographics tcu-pad-lr0 tcu-position--relative cf">

	<!-- SVG Top Angles -->
	<div class="tcu-infographics--tangles"></div>

	<div class="tcu-layout-constrain tcu-infographics cf">

		<?php
		// Check if the flexible content field has rows of data & loop through rows.
		if ( have_rows( 'infographics_section_icons' ) ) :

			/**
			 * Start the ACF loop.
			 */
			while ( have_rows( 'infographics_section_icons' ) ) :
				the_row();

				$tcu_icon     = get_sub_field( 'infographics_section_image' );
				$tcu_alt_text = get_sub_field( 'infographics_section_alt_text' );

				$tcu_html  = '<div class="tcu-graphics cf">';
				$tcu_html .= '<img src="' . esc_url( $tcu_icon['url'] ) . '" alt="' . esc_attr( $tcu_icon['alt'] ) . '"  /></div>';

				echo wp_kses_post( $tcu_html );

				/**
				 * End the ACF loop.
				 */
			endwhile;

		endif;
		?>
	</div><!-- end of .tcu-infographics -->


	<!-- SVG Bottom Angles -->
	<div class="tcu-infographics--bangles"></div>

	</div><!-- end of .tcu-background-infographics -->

</div><!-- end of .tcu-layout--large -->
