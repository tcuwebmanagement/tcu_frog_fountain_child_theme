<?php
/**
 * The template for displaying a message that posts cannot be found
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

?>
<div class="tcu-layoutwrap--transparent tcu-pad-lr0 cf">
	<div class="tcu-layout-constrain tcu-infographics cf">
		<article class="tcu-article hentry cf" role="article">
			<header class="tcu-article__header">
				<h2 class="h1"><?php esc_html_e( 'Oops, Post Not Found!', 'tcu_frog_fountain_child_theme' ); ?></h2>
			</header>
			<section class="tcu-article__content" role="region">
				<p><?php esc_html_e( 'Uh Oh. Something is missing. Try double checking things.', 'tcu_frog_fountain_child_theme' ); ?></p>
			</section>
		</article>
	</div>
</div>
