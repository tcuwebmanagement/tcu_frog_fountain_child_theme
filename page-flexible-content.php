<?php
/**
 * Template Name: Flexible Content
 *
 * @package tcu_frog_fountain_child_theme
 * @since TCU Frog Fountain Child Theme 1.0.0
 */

get_header();

// TCU Alert.
if ( get_field( 'tcu_alert_display_alert_message', 'option' ) ) {
	get_template_part( 'partials/content', 'alert' );
}

if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
	tcu_breadcrumbs_list(); } ?>

	<main id="main" role="main">

		<?php
		// Check if the flexible content field has rows of data.
		if ( have_rows( 'flexible_layout_sections' ) ) :

			// Loop through the rows of data.
			while ( have_rows( 'flexible_layout_sections' ) ) :
				the_row();

				// Check if current row layout.
				if ( 'large_hero_image_section' === get_row_layout() ) :

					get_template_part( 'partials/content', 'heroimage' );

					elseif ( 'large_video_section' === get_row_layout() ) :

						get_template_part( 'partials/content', 'video' );

					elseif ( 'highlight_sections_two_columns' === get_row_layout() ) :

						get_template_part( 'partials/content', 'highlightbox' );

					elseif ( 'highlight_sections_three_columns' === get_row_layout() ) :

						get_template_part( 'partials/content', 'threecards' );

					elseif ( 'expandable_banner' === get_row_layout() ) :

						get_template_part( 'partials/content', 'expandablebanner' );

					elseif ( 'infographics_section' === get_row_layout() ) :

						get_template_part( 'partials/content', 'infographics' );

					elseif ( 'two_column_section' === get_row_layout() ) :

						get_template_part( 'partials/content', 'columns' );

					elseif ( 'tabs_section' === get_row_layout() ) :

						get_template_part( 'partials/content', 'tabsection' );

					elseif ( 'six_block_section' === get_row_layout() ) :

						get_template_part( 'partials/content', 'sixblocks' );

					elseif ( 'active_calendar_feed' === get_row_layout() ) :

						get_template_part( 'partials/content', 'calendarfeed' );

					elseif ( 'tcu_news_section' === get_row_layout() ) :

						get_template_part( 'partials/content', 'tcunews' );

					elseif ( 'large_slider_section' === get_row_layout() ) :

						get_template_part( 'partials/content', 'slider' );

					elseif ( 'frog_fountain_visual_editor' === get_row_layout() ) :

						get_template_part( 'partials/content', 'visualeditor' );

					endif;

				endwhile;
				?>

			</main><!-- end of #main -->

			<?php else :

				get_template_part( 'partials/content', 'none' );

			endif;

get_footer(); ?>
