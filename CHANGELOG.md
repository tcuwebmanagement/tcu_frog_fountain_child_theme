## TCU Frog Fountain Child Theme

Author: Mayra Perales <mailto:m.j.perales@tcu.edu>

---

**v1.1.5**

- Fix tabcordion IDs to be unique.

**v1.1.4**

- Fix missing href within Slider.

**v1.1.3**

-   Fix masonry.js
-   Added TCU Alert WP Tab. It was deleted by error.

**v1.1.2**

-   Style for High Contrast Mode in IE

**v1.1.1**

-   Fix undefined tcu_image error when video section is missing

**v1.1.0**

-   Add hover/focus styles to close the alert section
-   Add ESLint, PHPCS, SassLint
-   Fix all linting issues to comply with WordPress Coding Standards
-   Update Slick.js to v1.8.0
-   Styled the What2do RSS widget
-   Removed redundant aria roles
-   Removed focus from SVG icons, bug on IE9
-   Remove the yellow active color for the transparent button
-   Update leadership template with email and phone number fields
-   Leadership template: Link, email, url are optional
-   Leadership template: Centered items and added space-between
-   Added a mobile background image for the video section
-   Expandable banner: Toggle aria-expanded, text, and inlined down/up arrows
-   Alert section: toggle aria-expand on click event
-   Close expandable banner on ESC keydown

**v1.0.2**

-   Added aria-label inputs to flexible-content template
-   Content area is option for slider, hero image and video section
-   Slider autoplays
-   Footer widgets are enabled
-   All buttons/links have focus styling
-   Expandable banner items are centered
-   JS loads only in flexible-content template
-   Ajax loads only for faculty-staff post type
-   Removed Active Calendar Feed section from flexible-content
-   Removed Two Column Section from flexible-content
-   Removed the tcu-breadcrumbs.php file
-   Added a close button to the TCU Alert message
-   Added masonry.js to the TCU News flexible content section

**v1.0.1**

-   Fixed expandable banner's empty button
-   Changed text/arrow color in the expandable banner
-   Removed heading tag is expandable banner links
-   Added .cf to .tcu-layout-center container in highlight cards
-   Modified headings in each flexible content section
-   Added hover color to Six Block section buttons
-   Fixed broken skip-nav link

**v1.0.0**

-   removed purple background in footer
-   ready for production

**v0.0.1**

-   Initial theme
-   Testing all flexible blocks
